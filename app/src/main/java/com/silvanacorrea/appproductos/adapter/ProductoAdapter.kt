package com.silvanacorrea.appproductos.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.silvanacorrea.appproductos.model.ProductEntity
import android.widget.TextView
import android.view.LayoutInflater
import android.widget.ImageView
import com.silvanacorrea.appproductos.R

class ProductoAdapter(private val context:Context, private val producto:List<ProductEntity>):BaseAdapter(){
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater = LayoutInflater.from(context)
        val container = inflater.inflate(R.layout.row_producto,null)
        val imgP = container.findViewById<ImageView>(R.id.ivProducto)
        val tvNombre = container.findViewById<TextView>(R.id.tvNombre)
        val tvPrecio = container.findViewById<TextView>(R.id.tvPrecio)

        val productEntity = producto[position]

        tvNombre.text = productEntity.nombre
        tvPrecio.text = productEntity.precio
        imgP.setImageResource(productEntity.foto)

        return  container
    }

    override fun getItem(position: Int)=producto[position]

    override fun getItemId(position: Int): Long = 0

    override fun getCount(): Int = producto.size

}