package com.silvanacorrea.appproductos.model

import  java.io.Serializable

data class ProductEntity(val id: Int, val nombre: String, val precio: String, val descripcion: String, val foto: Int, val fotoProducto: Int): Serializable