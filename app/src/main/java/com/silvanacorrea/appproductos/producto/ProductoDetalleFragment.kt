package com.silvanacorrea.appproductos.producto

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.silvanacorrea.appproductos.R
import com.silvanacorrea.appproductos.model.ProductEntity
import kotlinx.android.synthetic.main.fragment_producto_detalle.*
import java.lang.RuntimeException


class ProductoDetalleFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var listener: OnProductoListener? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_producto_detalle, container, false)
    }

    fun renderProducto(productEntity: ProductEntity){
        val nombreProductoD = productEntity.nombre
        val precioProductoD = productEntity.precio
        val descripcioProductoD = productEntity.descripcion

        tvProductoDetalle.text = nombreProductoD
        tvPrecioDetalle.text = precioProductoD
        tvDescripcionDetalle.text = descripcioProductoD

        ivProductoDetalle.setImageResource(productEntity.fotoProducto)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnProductoListener){
            listener = context
        }else{
            throw RuntimeException("$context necesita implementar OnProductoListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}