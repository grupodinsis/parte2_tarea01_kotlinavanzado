package com.silvanacorrea.appproductos.producto


import android.os.Bundle
import androidx.fragment.app.FragmentManager
import com.silvanacorrea.appproductos.R
import androidx.appcompat.app.AppCompatActivity
import com.silvanacorrea.appproductos.model.ProductEntity

class ProductoActivity :  AppCompatActivity(), OnProductoListener{

    private lateinit var productoFragment: ProductoFragment
    private lateinit var productoDetalleFragment: ProductoDetalleFragment
    private lateinit var fragmentManager: FragmentManager


    override fun selectedItemProducto(productEntity: ProductEntity) {
        productoDetalleFragment.renderProducto(productEntity)
    }

    override fun renderFirst(productEntity: ProductEntity?) {
        productEntity?.let {
            selectedItemProducto(it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_producto)

        fragmentManager = supportFragmentManager
        productoFragment = fragmentManager.findFragmentById(R.id.fragProducto) as ProductoFragment
        productoDetalleFragment = fragmentManager.findFragmentById(R.id.fragProductoDetalle) as ProductoDetalleFragment
    }


}