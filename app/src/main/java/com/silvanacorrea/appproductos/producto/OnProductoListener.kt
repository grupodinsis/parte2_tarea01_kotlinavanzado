package com.silvanacorrea.appproductos.producto

import com.silvanacorrea.appproductos.model.ProductEntity

interface OnProductoListener {

    fun selectedItemProducto(productEntity: ProductEntity)
    fun renderFirst(productEntity: ProductEntity?)
}