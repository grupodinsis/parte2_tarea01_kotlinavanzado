package com.silvanacorrea.appproductos.producto

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.silvanacorrea.appproductos.R
import com.silvanacorrea.appproductos.adapter.ProductoAdapter
import com.silvanacorrea.appproductos.model.ProductEntity
import kotlinx.android.synthetic.main.fragment_producto.*


class ProductoFragment : Fragment() {

    private var listener: OnProductoListener? = null
    private var producto = mutableListOf<ProductEntity>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_producto, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnProductoListener){
            listener = context
        }else{
            throw RuntimeException("$context necesita implementar OnProductosListener")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
         setData()

        lstProducto.adapter = ProductoAdapter(requireContext(), producto)

        lstProducto.setOnItemClickListener{ _, _, i, _ ->
            listener?.let {
                it.selectedItemProducto(producto[i])
            }

        }

        listener?.renderFirst(first())
    }

    private fun setData(){
        val producto1 = ProductEntity(1, "Pimiento", "S/.1.50", "Lorem ipum", R.mipmap.person,R.mipmap.pimientos)
        val producto2 = ProductEntity(2, "Carne", "S/.8.50", "Lorem ipum", R.mipmap.person,R.mipmap.carne)
        val producto3 = ProductEntity(3, "Piña", "S/.8.50", "Lorem ipum", R.mipmap.person,R.mipmap.pina)


        producto.add(producto1)
        producto.add(producto2)
        producto.add(producto3)
    }

    private fun first(): ProductEntity? = producto?.first()

    override fun onDetach() {
        super.onDetach()

        listener = null
    }
}
