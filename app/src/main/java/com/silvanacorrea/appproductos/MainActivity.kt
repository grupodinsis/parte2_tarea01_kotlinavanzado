package com.silvanacorrea.appproductos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.silvanacorrea.appproductos.producto.ProductoActivity

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        next(ProductoActivity::class.java,null,true)
    }
}